import java.util.Scanner;

public class SkanerZnakow {
    public static String wczytajZnak() {
        Scanner scan = new Scanner(System.in);
        String wpisanyZnak = scan.nextLine();
        boolean czyZnakJestPoprawny = sprawdzCzyZnakJestPoprawny(wpisanyZnak);
        while (!czyZnakJestPoprawny) {
            System.out.print("Wspierane znaki to ‘', ‘_’, ‘*’ oraz '/’. Wpisz poprawny znak: ");
            wpisanyZnak = scan.nextLine();
            czyZnakJestPoprawny = sprawdzCzyZnakJestPoprawny(wpisanyZnak);
        }
        return wpisanyZnak;
    }

    private static boolean sprawdzCzyZnakJestPoprawny(String znakDoSprawdzenia) {
        return znakDoSprawdzenia.equals("+")
                || znakDoSprawdzenia.equals("-")
                || znakDoSprawdzenia.equals("*")
                || znakDoSprawdzenia.equals("/");
    }
}
