public class Kalkulator {

    public static double oblicz(double wartosc1, double wartosc2, String znak) {
        if (znak.equals("+"))
            return dodaj(wartosc1, wartosc2);
        if (znak.equals("-"))
            return odejmi(wartosc1, wartosc2);
        if (znak.equals("/"))
            return podziel(wartosc1, wartosc2);
        if (znak.equals("*"))
            return pomnoz(wartosc1, wartosc2);
        return 0.0;
    }

    static double dodaj(double wartosc1, double wartosc2) {
        return wartosc1 + wartosc2;
    }

    static double odejmi(double wartosc1, double wartosc2) {
        return wartosc1 - wartosc2;
    }

    static double pomnoz(double wartosc1, double wartosc2) {
        return wartosc1 * wartosc2;
    }

    static double podziel(double wartosc1, double wartosc2) {
        if (rozneOdZera(wartosc2)) {
            return wartosc1 / wartosc2;
        } else {
            System.out.println("Łooooo Panie, co jest kurwa, eee!");
        }
        return 0.0;
    }

    private static boolean rozneOdZera(double wartosc) {
        return wartosc != 0.0;
    }
}
