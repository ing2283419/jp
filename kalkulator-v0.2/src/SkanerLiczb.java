import java.util.Scanner;

public class SkanerLiczb {
    public static Double wczytajLiczbe() {
        Scanner scan = new Scanner(System.in); // tworzymy obiekt czytający z konsoli
        String wpisanaWartosc = scan.nextLine(); // czytamy z konsoli wpisaną wartość, czytamy stringa, czyli tekst, ponieważ użytkownik może wprowadzić cokolwiek
        boolean czyJestPoprawnaLiczba = sprawdzCzyJestLiczba(wpisanaWartosc); // sprawdzamy czy podana wartość jest poprawna i wynik tego sprawdzenia zapisyjemy w czyJestPoprawnaLiczba
        while (czyJestPoprawnaLiczba == false) { // jeżeli czyJestPoprawnaLiczba == fale czyli nie jest prawdą, czyli podana wartość nie jest poprawną liczbą, to wchodzimy do pętli
            System.out.print("Podana wartość nie jest liczbą. Wpisz ponownie liczbę: "); // wyświetlamy informację, że podana wartość nie jest liczbą i prosimy o kolejne wpisanie wartości
            wpisanaWartosc = scan.nextLine(); // czekamy, aż użtkownik wpisze coś w konsoli i zatwierdzi enterem
            czyJestPoprawnaLiczba = sprawdzCzyJestLiczba(wpisanaWartosc); // jeszcze raz sprawdzamy czy wpisana wartość jest liczbą i wynik tego sprawdzenia zapisyjemy w czyJestPoprawnaLiczba
        }
        return Double.valueOf(wpisanaWartosc); // jeżeli wpisana wartość jest poprawną liczbą, to zwracamy ją jaki liczbę w typie Double
    }

    private static boolean sprawdzCzyJestLiczba(String wartoscDoSprawdzenia) {
        try {
            Double.valueOf(wartoscDoSprawdzenia); // sprawdzmy, czy podany tekst może być przekształcony w liczbę, jeżeli może być metoda Double.valueOf wykona się bez żadnego błędu, natomiast jeżeli nie, to zostanie rzucony błąd
            return true;
        } catch (NumberFormatException exception) {
            // jeżeli zostanie rzucony błąd, to nie chcemy, aby nasz program się wyłączył, tylko jest to błąd (wyjątek) który chcemy wykorzystać
            // wiemy, że jak zostanie rzucony, to oznacza, że podany tekst nie może być zamieniony na liczbę, czyli nie jest poprawną liczbą
            return false;
        }
    }
}
