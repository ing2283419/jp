public class Main {

    public static void main(String[] args) {
        System.out.println("Wpisz równanie:");

        System.out.print("Podaj pierwszą liczbę: ");
        Double wartosc1 = SkanerLiczb.wczytajLiczbe();

        System.out.print("Podaj znak: ");
        String znak = SkanerZnakow.wczytajZnak();

        System.out.print("Podaj drugą liczbę: ");
        Double wartosc2 = SkanerLiczb.wczytajLiczbe();

        System.out.println("Wynik to: " + wartosc1 + " " + znak + " " + wartosc2 + " = " + Kalkulator.oblicz(wartosc1, wartosc2, znak));
    }

}