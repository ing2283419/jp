import java.util.Scanner;
import java.util.Set;

public class SkanerZnakow {

    private final static Set<String> DOZWOLONE_ZNAKI = Set.of("+", "-", "*", "/");

    public static String wczytajZnak() {
        Scanner scan = new Scanner(System.in);
        String wpisanyZnak = scan.nextLine();
        boolean czyZnakJestPoprawny = sprawdzCzyZnakJestPoprawny(wpisanyZnak);
        while (!czyZnakJestPoprawny) {
            System.out.print("Wspierane znaki to ‘', ‘_’, ‘*’ oraz '/’. Wpisz poprawny znak: ");
            wpisanyZnak = scan.nextLine();
            czyZnakJestPoprawny = sprawdzCzyZnakJestPoprawny(wpisanyZnak);
        }
        return wpisanyZnak;
    }

    private static boolean sprawdzCzyZnakJestPoprawny(String znakDoSprawdzenia) {
        return DOZWOLONE_ZNAKI.contains(znakDoSprawdzenia);
    }
}
