import java.util.*;

public class CollectionsMain {
    public static void main(String[] args) {
        przykladListy();
        przykladSetu();
        przykladMapy();
    }

    private static void przykladMapy() {
        Map<String, String> pelnaNazwa = new HashMap<>();
        pelnaNazwa.put("Gocz", "Paulina");
        pelnaNazwa.put("Kusienicki", "Wojtek");

        System.out.println(pelnaNazwa);
        System.out.println(pelnaNazwa.get("Gocz"));
        System.out.println(pelnaNazwa.get("Kusienicki"));

        Map<String, List<String>> nazwiskoDoImion = new HashMap<>();
        nazwiskoDoImion.put("Gocz", List.of("Mateusz", "Paulina"));
        nazwiskoDoImion.put("Kusienicki", List.of("Wojtek"));

        System.out.println(nazwiskoDoImion);
        System.out.println(nazwiskoDoImion.get("Gocz").get(1));
        System.out.println(nazwiskoDoImion.get("Kusienicki"));

        System.out.println("Ilosc elementów w mapie: " + nazwiskoDoImion.size());
    }

    private static void przykladSetu() {
        Set<String> imiona = new HashSet<>();

        imiona.add("Mateusz");
        imiona.add("Wojtek");
        imiona.add("Paulina");

        System.out.println(imiona);

        System.out.println("Ilosc elementów w secie: " + imiona.size());
    }

    private static void przykladListy() {
        List<String> nazwiska = new ArrayList<>();

        nazwiska.add("Gocz");
        nazwiska.add("Dzialuk");
        nazwiska.add("Kusienicki");
        System.out.println(nazwiska);

        System.out.println(nazwiska.get(0));
        System.out.println(nazwiska.get(1));
        System.out.println(nazwiska.get(2));

        System.out.println(nazwiska.contains("Gocz"));
        System.out.println(nazwiska.contains("Smolka"));

        nazwiska.stream().filter(nazwisko -> {
            return nazwisko.contains("u");
        }).forEach(nazwisko -> {
            System.out.println(nazwisko);
        });

        System.out.println("Ilosc elementów w liscie: " + nazwiska.size());
    }

}