import java.util.ArrayList;
import java.util.List;

public class CollectionsTask {

    private final static List<String> NAMES = List.of(
            "Loba",
            "Bangalore",
            "Lifeline",
            "Pathfinder"
    );

    public static void main(String[] args) {
        displayNamesInForEach();
        displayNamesInFor();
        displayNamesInForIn();
        displayNamesInWhile();
    }

    public static void displayNamesInForEach() {
        List<String> names = generateListOfNames();

        System.out.println("Display using forEach");
        names.forEach(name -> System.out.println(name));
        System.out.println();
    }

    public static void displayNamesInFor() {
        List<String> names = generateListOfNames();

        System.out.println("Display using classic for");
        for (int i = 0; i < names.size(); i++) {
            System.out.println(names.get(i));
        }
        System.out.println();
    }

    public static void displayNamesInForIn() {
        List<String> names = generateListOfNames();

        System.out.println("Display using for in");
        for (String name : names) {
            System.out.println(name);
        }
        System.out.println();
    }

    public static void displayNamesInWhile() {
        List<String> names = generateListOfNames();

        System.out.println("Display using old while - worst solution in this case");
        int i = 0;
        while (i < names.size()) {
            System.out.println(names.get(i));
            i++;
        }
        System.out.println();
    }

    private static List<String> generateListOfNames() {
        List<String> names = new ArrayList<>();

        names.add("Loba");
        names.add("Bangalore");
        names.add("Lifeline");
        names.add("Pathfinder");

        return names;
    }
}
